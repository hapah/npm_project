module.exports = function(grunt) {
    var date = '<%= grunt.template.today("yyyymmddHHMMss") %>'

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        includeSource: {
            options: {
                basePath: 'dist',
                baseUrl: '/'
            },
            myTarget: {
                files: {
                    'dist/index.html': 'dist/index.html'
                }
            }
        },
        
        copy: {
            files: {
                expand: true,
                dest: 'dist',
                cwd: 'src',
                src: 'index.html'
            }
        },

        exec: {
            clean: 'mkdir -p dist && rm -rf dist/*'
        },

        watch: {
            all: {
                files: ['src/*'],
                tasks: ['clean', 'build'],
                options: {
                    livereload: true
                }
            }
        },
    
        concat: {
            js: {
                src: ['node_modules/jquery/dist/jquery.min.js', 'src/app.js'],
                dest: 'dist/built.js'
            },
            css: {
                src: ['node_modules/bootstrap/dist/css/bootstrap.min.css', 'node_modules/bootstrap/dist/css/bootstrap-theme.min.css', 'src/main.css'],
                dest: 'dist/main.css'
            }
        },
    
        uglify: {
            build: {
                src: 'dist/built.js',
                dest: 'dist/built' + date + '.min.js'
            }
        },
        
        cssmin: {
            build: {
                src: 'dist/main.css',
                dest: 'dist/main' + date + '.min.css'
            }
        }

    });
    
    // Default task
    grunt.registerTask('default', ['watch']);
    grunt.registerTask('clean', ['exec']);
    grunt.registerTask('build', ['concat', 'uglify', 'cssmin', 'copy', 'includeSource']);

    // Load up tasks
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-include-source');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-exec');
    
};