var gulp = require('gulp'),
gutil = require('gulp-util'),
connect = require('gulp-connect'),
uglify = require('gulp-uglify'),
cleanCSS = require('gulp-clean-css'),
concat = require('gulp-concat'),
es = require('event-stream'),
inject = require('gulp-inject'),
rev = require('gulp-rev');

var jsSources = ['node_modules/jquery/dist/jquery.min.js', 'src/app.js'],
cssSources = ['node_modules/bootstrap/dist/css/bootstrap.min.css', 'node_modules/bootstrap/dist/css/bootstrap-theme.min.css', 'src/main.css'],
inputDir = 'src/*',
outputDir = 'dist';


gulp.task('log', function() {
    gutil.log('== My First Task ==')
});

gulp.task('copy', function() {
    gulp.src('src/index.html')
    .pipe(gulp.dest(outputDir))
});

gulp.task('build', function() {
    var jsStream = gulp.src(jsSources)
    .pipe(uglify())
    .pipe(concat('script.js'))
    .pipe(rev())
    .pipe(gulp.dest(outputDir))
    .pipe(connect.reload())

    var cssStream = gulp.src(cssSources)
    .pipe(cleanCSS())
    .pipe(concat('style.css'))
    .pipe(rev())
    .pipe(gulp.dest(outputDir))
    .pipe(connect.reload())

    gulp.src('src/index.html')
    .pipe(inject(es.merge(jsStream, cssStream)))
    .pipe(gulp.dest('dist'))
    .pipe(connect.reload())
});

gulp.task('watch', function() {
    gulp.watch(inputDir, ['clean', 'build'])
});

gulp.task('connect', function() {
    connect.server({
        root: '.',
        livereload: true
    })
});

var exec = require('child_process').exec;

gulp.task('clean', function (cb) {
 exec('mkdir -p dist && rm -rf dist/*', function (err, stdout, stderr) {
   console.log(stdout);
   console.log(stderr);
   cb(err);
 });
})

gulp.task('index', function () {
    var target = gulp.src('src/index.html');
    // It's not necessary to read the files (will speed up things), we're only after their paths:
    var sources = gulp.src(['dist/*.js', 'dist/*.css'], {read: false});

    return target.pipe(inject(sources))
        .pipe(gulp.dest('dist'));
});

gulp.task('default', ['watch', 'connect']);
