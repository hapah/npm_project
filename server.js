var express = require('express')
var app = express()
var path = require('path')
var proxy = require('express-http-proxy')
var url = require('url')
var moment = require('moment')
moment.locale('ru')

app.get('/data/date.json', function (req, res) {
   var date = moment().format('DD MMMM YYYY')
   var json = JSON.stringify({"date":date})
   res.end(json)
})

app.use(express.static(__dirname + '/src/app.js'))
app.use(express.static(__dirname + '/src/main.css'))
app.use('/dist', express.static(__dirname + '/dist'))

app.use('/', proxy('www.aisa.ru', {
    filter: function(req, res) {
        return (req.url == '/images/round_logo@2x.png' || req.url == '/logo.png')
    },
    proxyReqPathResolver: function(req, res) {
        if (req.url == '/images/round_logo@2x.png' || req.url == '/logo.png')
            return url.parse('/images/round_logo@2x.png').path
    }
}))

app.set('view engine', 'ejs')

app.get('/', function (req, res) { 
    res.sendFile(__dirname + '/dist/index.html')
})

app.listen(8181, function() {
    console.log('Server started')
})
