function changeToAlert() {
    $("#time").toggleClass("alert alert-warning well")
}
function updateTime() {
    $.ajax({ type: "GET",   
        url: "/data/date.json",   
        async: false,
        success : function(text) {
            var date = JSON.parse(text)
            $("#time").text(date['date'])
        }
    })
}